/*
 * Copyright (c) 2018 RuneScape Wiki team
 *
 * SPDX-License-Identifier: GPL-3.0
 */
import L from 'leaflet';
import 'leaflet-fullscreen';

import * as preferences from './preferences';

import * as controls from './map/controls';
import './map/components';
import * as labels from './map/labels';
import * as locations from './map/locations';
import * as pathfind from './map/pathfind';

import LayerDataLoader from './map/layers/LayerDataLoader';

export function bootstrap() {
  console.log('[MAP] init()');

  let domElem = document.getElementById('map');

  var mapCreator = new MapCreator();
  mapCreator.load(domElem);

}

class MapCreator {

  constructor(){
    this._controlers = {};
    this._map = null;
    this.config = {};
    this.fullscreen = true;
    this.layerDataLoader = new LayerDataLoader();
  }

  load(domElem){
    this.loadConfig().then(() => {
      this.setup(domElem);
    });
  }

  async loadConfig(){
    var mapCreator = this;
    return new Promise(async function(resolve, reject) {
      var responceData = await fetch('data/config.json');
      mapCreator.config = await responceData.json();
      resolve(mapCreator.config);
    });
  }

  setup(domElem) {

    // Create CRS (coordinate reference system)
    L.CRS.Simple.infinite = false;
    L.CRS.Simple.projection.bounds = new L.Bounds([ [0, 0], [12800, 12800] ]);

    var mapCreator = this;

    var RuneScapeMap = L.Map.extend( {

      // initialize: function ( id, options ) {
      //   L.Map.prototype.initialize.apply(this, {id, options});
      //   options = L.Util.setOptions(this, options);
      //   this._mapID = options.mapID || 0;
      //   this._plane = options.plane || 0;
      // },
      init: function(){
        this.on('planechanging', this._planeChanging, this);
        this.on('mapidchanging', this._mapIDChanging, this);
        this._plane = 0;
        this._mapID = 0;
      },

      initDefaultView: function(){
        // if no hash set map to Lumbridge
        this.setView([ 3225, 3219 ], 2);
      },

      initView: function( mapID, plane, center, zoom ){
        // Convert array to obj
        if ( Array.isArray( center ) ) {
          if ( !isNaN( center[ 0 ] ) && !isNaN( center[ 1 ] ) ) {
            center = L.latLng( center );
          } else {
            center = undefined;
          }
        }

        if ( 'mapControler' in mapCreator._controlers ) {
          mapCreator._controlers.mapControler.setInitView( mapID, plane, center, zoom );
        }
      },

      _planeChanging: function(e){
        this._plane = e.plane;

        this.fire('planechanged', {
          previous: e.current,
          plane: e.plane,
          userChanged: e.userChanged,
        });
      },

      _mapIDChanging: function(e){
        this._mapID = e.mapID;

        this.fire('mapidchanged', {
          previous: e.current,
          mapID: e.mapID,
          userChanged: e.userChanged,
        });
      },

      getMapID: function(){
        return this._mapID;
      },

      getPlane: function(){
        return this._plane;
      },
    });

    // Setup map
    this._map = new RuneScapeMap(domElem, {
      crs: L.CRS.Simple,
      maxBounds: [ [0, 0], [12800, 12800] ],
      maxBoundsViscosity: 0.5,
      zoomControl: false, // Replace default zoom controls with our own
      attributionControl: false, // Creating our own attribution Controler
      fullscreenControl: true,
    });
    this._map.init();

    // Setup controls
    this.setupControls(controls);
    this.layerDataLoader.setControlers(this._controlers);
    this.layerDataLoader.setConfig(this.config);
    this.layerDataLoader.load();

    // Setup map events
    this._map.on('load', () => {
      // labels.init();
      // pathfind.init(this);
    });
  }

  setupControls(controls) {
    var controler;
    this._map.fullscreenControl.setPosition('topright');
    // top left
    if ( !this.fullscreen ) {
      this._controlers.zoom = new controls.CustomZoom({ position: 'topleft', displayZoomLevel: false });
    }

    // top right
    if ( this.fullscreen ) {
      this._controlers.zoom = new controls.CustomZoom({ position: 'topright', displayZoomLevel: true });
      this._controlers.help = new controls.Help();
      this._controlers.icons = new controls.Icons();
      this._controlers.options = new controls.Options();
    }

    // bottom left
    this._controlers.mapSelect = new controls.MapSelect(
      { visible: this.fullscreen } );

    // bottom right
    this._controlers.attribution = L.control.attribution(
      { prefix: this.config.attribution } );
    this._controlers.plane = new controls.Plane(
      { visible: this.fullscreen });

    // add controlers to map
    for ( controler in this._controlers ) {
      this._controlers[ controler ].addTo( this._map );
    }

    // Don't add this to map
    this._controlers.mapControler = new controls.MapControler(this._map);
    this._controlers.hash = new L.Hash(this._map);

    // add class to bottom-right to make vertical
    this._map._container.querySelector( '.leaflet-control-container > .leaflet-bottom.leaflet-right' ).className += ' vertical';
  }
}
