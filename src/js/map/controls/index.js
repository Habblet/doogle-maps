
/*
 * This file and the files in this folder are intenialy formated in
 * ES5 and not ES6 this is for the shared codebase with
 * the Kartographer Extenstion in the wiki.
 */

// Next part is not used in Kartographer
// if KARTOGRAPHER=false
//const CustomZoom = require('./CustomZoom');
// module.Help = require('./Help');
// module.Icons = require('./Icons');
// module.Options = require('./Options');
// module.Plane = require('./Plane');
// module.MapSelect = require('./MapSelect');
export * from './uiElements';
export * from './CustomZoom';
export * from './Help';
export * from './Icons';
export * from './Options';
export * from './Plane';
export * from './MapSelect';
export * from './MapControler';
// endif

// if KARTOGRAPHER=true
// module.exports = {
//   CustomZoom: module.CustomZoom,
//   Help: module.Help,
//   Icons: module.Icons,
//   Options: module.Options,
//   Plane: module.Plane,
//   MapSelect: module.MapSelect,
// };
// endif
