var tabs = [];

/**
 * Makes working tab navigation links
 */
export function init() {
  console.log('[TABS] init()');

  let tabElems = document.getElementsByClassName('tabs');
  for(let tab of tabElems) {
    var group = tab.dataset.tabMenu || false;
    if(!group) {
      console.log('Found tab group with invalid name: ', tab);
      return;
    }

    let items = tab.querySelectorAll('.tabs__navigation li');
    for(let item of items) {
      item.dataset.tabGroup = group;
      item.addEventListener('click', function(e) { enableTab(e.currentTarget || e.target); }, false);
    }

    tabs[group] = null;

    var lastOpenedTab = false;
    if(lastOpenedTab) {
      enableTab(tab.querySelector('li[data-tab=\'' + lastOpenedTab + '\']'));
    } else {
      enableTab(items[0]);
    }
  }
}

function enableTab(item) {
  return configureTab(item, true);
}

function disableTab(item) {
  return configureTab(item, false);
}

function configureTab(item, enable) {
  if(!item) {
    return;
  }

  var tabGroup = item.dataset.tabGroup || false;
  if(!tabGroup) {
    console.log('Item has invalid tab group: ', item);
    return;
  }

  var tabName = item.dataset.tab || false;
  if(!tabName) {
    console.log('Item has invalid tab name: ', item);
    return;
  }

  // Skip if this is the active tab already
  if(enable && tabs[tabGroup] == item) {
    return;
  }
	
  var content = document.getElementById('tab-' + tabGroup + '-' + tabName);
  if(!content) {
    console.log('Unable to find tab for: ' + tabGroup + '@' + tabName);
    return;
  }

  // Disable previous tab
  if(enable && tabs[tabGroup]) {
    disableTab(tabs[tabGroup]);
    tabs[tabGroup] = null;
  }

  if(enable) {
    item.classList.add('active');
    content.classList.add('active');
  } else {
    item.classList.remove('active');
    content.classList.remove('active');
  }

  // console.log((enable ? 'Enabled' : 'Disabled') +" tab: " + tabGroup + "@" + tabName);

  // Update reference to active tab
  tabs[tabGroup] = item;
}