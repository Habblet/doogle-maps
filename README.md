# RuneScape Wiki Maps Project
We are creating a dynamic, interactive RuneScape map that can be embedded in wiki articles. It will support zooming, panning, markup (labels, markers, paths, icons), and links between dungeon maps. This map will also be available as a standalone application.

# Useful links
* [Main info](https://oldschool.runescape.wiki/w/RuneScape:Map)

# State of the art
The following technologies are being used in this project.

| Asset | Name | Links |
| --- | --- | --- |
| HTML | [Nunjucks](https://mozilla.github.io/nunjucks/) | [Docs](https://mozilla.github.io/nunjucks/templating.html) |
| CSS | [Sass](https://sass-lang.com/) |[Getting Started](https://sass-lang.com/guide) • [Docs](https://sass-lang.com/documentation/file.SASS_REFERENCE.html) |
| JS | [ES6+](http://ecmascript.org/)  | [What's New in ES6](http://exploringjs.com/es6/)  |

# Installation
To build the assets in this project you'll need [Node.js](https://nodejs.org/en/download/]).
## Initial setup
Run `npm install`. This will install all dependencies needed.

I might be needed to set your environment to development. This will make sure to download the development dependencies.
`npm config set dev true` or use `npm install --only=dev`.

## Building assets
Every time you modify any asset (found in the `src/` directory), the assets will need to be rebuilt.
* To build the assets, execute `npm run build`
*  All the assets will be available in the `dist/` directory

During development `npm run dev` can also be used. Files will be watched for changes and rebuild.
